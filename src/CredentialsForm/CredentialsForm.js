import React from 'react';
import AuthService from '../AuthService';

import classes from './CredentialsForm.css';

class CredentialsForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loggedIn: false
        };
        this.domain = this.props.domain;
        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.Auth = new AuthService(this.props.domain);
    }

    componentWillMount() {
        if (!this.Auth.loggedIn()) {
            this.setState({loggedIn: false})
        }
        else {
            try {
                const profile = this.Auth.getProfile()
                this.setState({
                    user: profile,
                    loggedIn: true
                })
            }
            catch(err){
                this.Auth.logout()
                this.setState({loggedIn: false})
            }
        }

    }

    handleChange(e){
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    }

    async handleFormSubmit(e){

        e.preventDefault();
      
        await this.Auth.fetch(`${this.domain}/users/credentials`, {
            method: 'PUT',
            body: JSON.stringify({
                firstname: this.state.firstname,
                lastname: this.state.lastname,
                email: this.state.email,
                phoneNumber: this.state.phoneNumber,
                sellerID: this.state.sellerID,
                accessKey: this.state.accessKey,
                authToken: this.state.authToken,
                marketplaceID: this.state.marketplaceID,
                AWS_SECRET_ACCESS_KEY: this.state.AWS_SECRET_ACCESS_KEY
            })
        });
        this.setState({
            firstname: null,
            lastname: null,
            email: null,
            phoneNumber: null,
            sellerID: null,
            accessKey: null,
            authToken: null,
            marketplaceID: null,
            AWS_SECRET_ACCESS_KEY: null
        })
    }
    
    render() {

        return (
            <form className={classes.card} onSubmit={this.handleFormSubmit}>
                <p className={classes.p}>Personal Information:</p>
                <div className={classes.inputDiv}>
                    <label>First Name:</label>
                    <input className={classes.input} type="text" name="firstname" placeholder="First Name" onChange={this.handleChange}/>
                </div>
                <div className={classes.inputDiv}>
                    <label>Last Name:</label>
                    <input className={classes.input} type="text" name="lastname" placeholder="Last Name" onChange={this.handleChange}/>
                </div>
                <div className={classes.inputDiv}>
                    <label>Email:</label>
                    <input className={classes.input} type="text" name="email" placeholder="Email" onChange={this.handleChange}/>
                </div>
                <div className={classes.inputDiv}>
                    <label>Phone:</label>
                    <input className={classes.input} type="text" name="phoneNumber" placeholder="Phone" onChange={this.handleChange}/>
                </div>
                <p className={classes.p}>Amazon Seller Account Information:</p>
                <div className={classes.inputDiv}>
                    <label>Seller ID:</label>
                    <input className={classes.input} type="text" name="sellerID" placeholder="Seller ID" onChange={this.handleChange}/>
                </div>
                {/* <div className={classes.inputDiv}>
                    <label>Access Key:</label>
                    <input className={classes.input} type="text" name="accessKey" placeholder="Access Key" onChange={this.handleChange}/>
                </div> */}
                <div className={classes.inputDiv}>
                    <label>Auth Token:</label>
                    <input className={classes.input} type="text" name="authToken" placeholder="Auth Token" onChange={this.handleChange}/>
                </div>
                <div className={classes.inputDiv}>
                    <label>Marketplace ID:</label>
                    <input className={classes.input} type="text" name="marketplaceID" placeholder="Marketplace ID" onChange={this.handleChange}/>
                </div>
                {/* <div className={classes.inputDiv}>
                    <label>AWS Secret Access Key:</label>
                    <input className={classes.input} type="text" name="AWS_SECRET_ACCESS_KEY" placeholder="AWS Secret Access Key" onChange={this.handleChange}/>
                </div> */}
                <div className={classes.btnDiv}>
                    <button className={classes.button} type="submit">Submit</button>
                </div>
            </form>
            
        )
    }
    
}

export default CredentialsForm;