import React from 'react';

import TrackedAsin from './TrackedAsin/TrackedAsin';

const trackedAsins = (props) => props.trackedAsins.map((item, index) => {
    return (
      <TrackedAsin key={item._id}
        id={item._id}
        image={item.image}
        index={index}
        price={item.price}
        title={item.title}
        details={item.details}
        asin={item.asin}
        hours={item.hours}
        minPrice={item.minPrice}
        click={props.stopTrackedAsinHandler}/>
    )
  })

export default trackedAsins;