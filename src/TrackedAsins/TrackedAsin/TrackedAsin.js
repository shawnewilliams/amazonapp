import React from 'react';
import classes from './TrackedAsin.css';

const trackedAsin = (props) => {
    return (
        <div className={classes.card}>
            <h4 className={classes.asin}>ASIN: {props.asin}</h4>
            
            <p className={classes.message} >Price Threshold: ${props.minPrice}</p>
            {/* <p className={classes.message} >Alert Interval: {props.hours} hours</p> */}

            {/* <div className={classes.details}>
                <img className={classes.smallImage} src={props.image} />
                <p className={classes.p}>{props.title}</p>
            </div> */}

            <iframe className={classes.iframe}  scrolling="no" frameBorder="0" title={`Amazon Product ${props.asin}`} src={`//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=108f29-20&marketplace=amazon&region=US&placement=${props.asin}&asins=${props.asin}&linkId=903821764170d5545bd428917461f09e&show_border=false&link_opens_in_new_window=true&price_color=000000&title_color=0066C0&bg_color=FFFFFF`}></iframe>

            <button className={classes.button} onClick={() => props.click(props.id)}>Stop</button>
        </div>
    )
}

export default trackedAsin;