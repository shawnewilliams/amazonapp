import React from 'react';
import classes from './Loading.css';
import FaAmazon from 'react-icons/lib/fa/amazon';


const loading = () => {
    return (
        <div>
            <FaAmazon className={classes.icon}/>
        </div>
    )
}

export default loading;