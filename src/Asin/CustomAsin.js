import React from 'react';
import axios from 'axios';
import MdAccessAlarm from 'react-icons/lib/md/access-alarm';
import MdAttachMoney from 'react-icons/lib/md/attach-money';

import classes from './Asin.css';

class CustomAsin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            asin: '',
            hours: '',
            minPrice: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        try {
          let toTrack = await this.props.getAsinsDetails(this.state.asin);
          console.log(toTrack);
          const instance = await axios.create({
            baseURL: 'http://localhost:3000/tracked-asin',
            // json: true
          });
          await instance({
            method:'post',
            url: '',
            data: {
              asin: toTrack.asin,
              image: toTrack.image,
              price: toTrack.price,
              title: toTrack.title,
              minPrice: this.state.minPrice,
              hours: this.state.hours
            }
          });
          await this.props.getTrackedAsins();
          const response = await axios.get(`http://localhost:3000/track/${this.state.asin}/${this.state.minPrice}/${this.state.hours * 3600000}/go/${this.state.asin}`);
          // this.trackedAsinHandler(this.state.asinCustom, this.state.minPriceCustom, this.state.hoursCustom);
          let newPrices = response.data;
          console.log(newPrices);
          this.setState({
            asin: '',
            minPrice: '',
            hours: ''
          });
        } catch (error) {
          this.setState({
          asin: '',
          minPrice: '',
          hours: ''
        });
        console.log(error);
        }
      };

    render() {
        return (
            <form className={classes.card} onSubmit={this.handleSubmit}>
    
                <div className={classes.inputDiv}>
                    <input className={classes.input} type="text" name="asin" value={this.state.asin} placeholder="ASIN" onChange={this.handleChange} />
                </div>
    
                <div className={classes.inputDiv}>
                    <span className={classes.icon}><MdAttachMoney /></span>
                    <input className={classes.input} type="text" name="minPrice" value={this.state.minPrice} placeholder="Min Price" onChange={this.handleChange} />
                </div>
    
                <div className={classes.inputDiv}>
                    <span className={classes.icon}><MdAccessAlarm /></span>
                    <input className={classes.input} type="text" name="hours" value={this.state.hours} placeholder="Hours" onChange={this.handleChange} />
                </div>
                
                <div className={classes.btnDiv}>
                    <button className={classes.button} type="submit">Start</button>
                </div>
            </form>
        )
    }
    
}
   
    


export default CustomAsin;