import React from 'react';
// import axios from 'axios';
import MdAccessAlarm from 'react-icons/lib/md/access-alarm';
import MdAttachMoney from 'react-icons/lib/md/attach-money';
import FaAmazon from 'react-icons/lib/fa/amazon';
import AuthService from '../AuthService';


import classes from './Asin.css';

class Asin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            asin: '',
            hours: '',
            minPrice: '',
        };
        this.domain = this.props.domain // API server domain
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.Auth = new AuthService(this.props.domain);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        try {
            let toTrack = await this.props.getAsinsDetails(this.state.asin);
            console.log(toTrack);
            let response = await this.Auth.fetch(`${this.domain}/tracked-asin`, {
            method: 'POST',
            body: JSON.stringify({
                asin: toTrack.asin,
                image: toTrack.image,
                price: toTrack.price,
                title: toTrack.title,
                minPrice: this.state.minPrice,
                hours: this.state.hours
                })
            });
       
            await this.props.getTrackedAsins();
            
            console.log(response);
            let newPrices = response;
            this.setState({
                asin: '',
                minPrice: '',
                hours: '',
            });
        } catch (error) {
            this.setState({
                asin: '',
                minPrice: '',
                hours: ''
            });
            console.log(error);
        }
    };
   
    render() {
        let options = (
            this.props.asinList.map((item, index) => {
                return  (
                    <option key={index} value={item.asin}>{`${item.asin} - ${item.title}`}</option>
                )
            })
        )
    
        return (
            <div>
                <p>Select an ASIN from your list of products</p>
                <form className={classes.card} onSubmit={this.handleSubmit}>
                
                    <div className={classes.asinSelectDiv}>
                        <span className={classes.iconAmazon}><FaAmazon /></span>
                        <label htmlFor="ASINS">ASINS</label>
                        <select className={classes.select} name="asin" id="asin" onChange={this.handleChange}>
                            <option value={this.state.asin}>ASIN</option>
                            {options}
                        </select>
                    </div>
                    
                    <div className={classes.inputDiv}>
                        <span className={classes.icon}><MdAttachMoney /></span>
                        <input className={classes.input} type="text" name="minPrice" value={this.state.minPrice} placeholder="Min Price" onChange={this.handleChange} />
                    </div>
        
                    <div className={classes.inputDiv}>
                        <span className={classes.icon}><MdAccessAlarm /></span>
                        <input className={classes.input} type="text" name="hours" value={this.state.hours} placeholder="Hours" onChange={this.handleChange} />
                    </div>
        
                    <div className={classes.btnDiv}>
                        <button className={classes.button} type="submit">Start</button>
                    </div>
                </form>
            </div>
        )
    }
    
}

export default Asin;