import React from 'react';
import AuthService from '../AuthService';

import classes from './Signup.css';

class Signup extends React.Component {
    constructor(props){
        super(props);
        
        this.state = {
            loggedIn: false,
            showSignup: false,
            password: '',
            username: '',
            email: '',
            phoneNumber: '',
            touched: {
                username: false,
                password: false,
                email: false,
                phoneNumber: false,
              },
        };

        this.domain = this.props.domain;
        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.Auth = new AuthService(this.props.domain);
    }

    componentWillMount() {
        this.loggedIn();

    }

    loggedIn() {
        if (!this.Auth.loggedIn()) {
            this.setState({loggedIn: false})
        }
        else {
            try {
                const profile = this.Auth.getProfile()
                this.setState({
                    user: profile,
                    loggedIn: true
                })
            }
            catch(err){
                this.Auth.logout()
                this.setState({loggedIn: false})
            }
        }
    }

    handleChange(e){
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    }

    handleFormSubmit(e){

        e.preventDefault();
      
        this.Auth.login(this.state.username,this.state.password)
            .then(res => {
                console.log(res);
            //    this.props.history.replace('/');
            console.log(this.Auth.isTokenExpired(res.token));
            console.log(this.Auth.getProfile(res.token));
            const profile = this.Auth.getProfile()
                this.setState({
                    user: profile,
                    loggedIn: true,
                    password: ''
                })            
            })
            .catch(err =>{
                alert(err);
            });  
    }

    handleSignup = async (e) => {
        e.preventDefault();
        try {
            await this.Auth.fetch(`${this.domain}/users/signup`, {
            method: 'POST',
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password
                })
            });
            await this.handleFormSubmit(e);

        } catch (e) {
            console.log(e);
        }
    }

    toggleSignup() {
        // console.log(this.state)
        let signup = this.state.showSignup;
        this.setState({showSignup: !signup});
    }

    handleLogout(e) {
        console.log(this.Auth.logout())
        this.Auth.logout();
        this.setState({
            loggedIn: false, 
            user: '', 
            username: '',
            touched: {
                username: false,
                password: false,
                email: false,
                phoneNumber: false,
              },
            });
    }

    validate(username, password) {
        // const { username, password } = this.state;
        // true means invalid, so our conditions got reversed
        return {
          username: username.length < 3,
          password: password.length < 3,
        };
    }

    handleBlur = (field) => (event) => {
        this.setState({
          touched: { ...this.state.touched, [field]: true },
        });
    }

    shouldMarkError = (field) => {
        const errors = this.validate(this.state.username, this.state.password);
        console.log(field);
        const hasError = errors[field];
        const shouldShow = this.state.touched[field];
        
        return hasError ? shouldShow : false;
    };
    
    render() {
        
        const { username, password } = this.state;
        // const isEnabled = username.length > 0 && password.length > 0;
        const errors = this.validate(username, password);
        const isEnabled = !Object.keys(errors).some(x => errors[x]);

        // let login = null;
        // if (!this.state.loggedIn && !this.state.showSignup) {
        //     login = (
        //         <form className={classes.form} onSubmit={this.handleFormSubmit}>
        //             <div className={classes.login}>
        //                 <label>Username:</label>
        //                 <input 
        //                     type="text" 
        //                     className={this.shouldMarkError('username') ? classes.error : ''}
        //                     name="username" placeholder="Username" 
        //                     onChange={this.handleChange}
        //                     onBlur={this.handleBlur('username')}/>
        //             </div>
        //             <div className={classes.login}>
        //                 <label>Password:</label>
        //                 <input type="password" 
        //                 className={this.shouldMarkError('password') ? classes.error : ''} 
        //                 name="password" placeholder="Password" 
        //                 onChange={this.handleChange}
        //                 onBlur={this.handleBlur('password')}/>
        //             </div>
        //             <div>
        //                 <button className={classes.loginButton} disabled={!isEnabled} type="submit">Login</button>
        //             </div>
        //             <p className={classes.showSignup} onClick={() => this.toggleSignup()}>Sign Up</p>
        //         </form>
        //     )
        // }

        let signup = null;
        if (!this.state.loggedIn && this.state.showSignup) {
            signup = (
                <form className={classes.card} onSubmit={this.handleFormSubmit}>
                <p className={classes.p}>Personal Information:</p>
                <div className={classes.inputDiv}>
                    <label>First Name:</label>
                    <input className={classes.input} type="text" name="firstname" placeholder="First Name" onChange={this.handleChange}/>
                </div>
                <div className={classes.inputDiv}>
                    <label>Last Name:</label>
                    <input className={classes.input} type="text" name="lastname" placeholder="Last Name" onChange={this.handleChange}/>
                </div>
                <div className={classes.inputDiv}>
                    <label>Email:</label>
                    <input className={classes.input} type="text" name="email" placeholder="Email" onChange={this.handleChange}/>
                </div>
                <div className={classes.inputDiv}>
                    <label>Phone:</label>
                    <input className={classes.input} type="text" name="phoneNumber" placeholder="Phone" onChange={this.handleChange}/>
                </div>
                <p className={classes.p}>Amazon Seller Account Information:</p>
                <div className={classes.inputDiv}>
                    <label>Seller ID:</label>
                    <input className={classes.input} type="text" name="sellerID" placeholder="Seller ID" onChange={this.handleChange}/>
                </div>
                {/* <div className={classes.inputDiv}>
                    <label>Access Key:</label>
                    <input className={classes.input} type="text" name="accessKey" placeholder="Access Key" onChange={this.handleChange}/>
                </div> */}
                <div className={classes.inputDiv}>
                    <label>Auth Token:</label>
                    <input className={classes.input} type="text" name="authToken" placeholder="Auth Token" onChange={this.handleChange}/>
                </div>
                <div className={classes.inputDiv}>
                    <label>Marketplace ID:</label>
                    <input className={classes.input} type="text" name="marketplaceID" placeholder="Marketplace ID" onChange={this.handleChange}/>
                </div>
                {/* <div className={classes.inputDiv}>
                    <label>AWS Secret Access Key:</label>
                    <input className={classes.input} type="text" name="AWS_SECRET_ACCESS_KEY" placeholder="AWS Secret Access Key" onChange={this.handleChange}/>
                </div> */}
                <div className={classes.btnDiv}>
                    <button className={classes.button} type="submit">Submit</button>
                </div>
            </form>
            )
        }

        return (
            <div>
                {signup}
                {/* {login} */}
                {/* {logout} */}
            </div>
            
        )
    }
    
}

export default Signup;