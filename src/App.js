import React, { Component } from 'react';
import Asin from './Asin/Asin';
import TrackedAsins from './TrackedAsins/TrackedAsins';
import SearchedProducts from './SearchedProducts/SearchedProducts';
import Login from './Login/Login';
import SearchForm from './SearchForm/SearchForm';
import Loading from './Loading/Loading';
import AuthService from './AuthService';
import CredentialsForm from './CredentialsForm/CredentialsForm';

import classes from './App.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.Auth = new AuthService();
    // this.domain = 'http://localhost:3000'
    this.domain = 'https://amazon-server.herokuapp.com'
    this.state = {
      asin: '',
      prices: [],
      asins: [],
      details: [],
      trackedAsins: '',
      searchResults: [],
      loading: false,
      showTracked: false,
      trackedMessage: 'Show Tracked',
      showCredentials: false
    }
  }

  resetState = () => {
    this.setState({
      asin: '',
      prices: [],
      asins: [],
      details: [],
      searchResults: [],
      trackedAsins: '',
      loading: false,
      showTracked: false,
      trackedMessage: 'Show Tracked',
      showCredentials: false,
    })
  }
  getAsinsList = async () => {
    try {
      await this.getTrackedAsins();
      // let result = await axios.get('http://localhost:3000/list-asins');
      let result = await this.Auth.fetch(`${this.domain}/list-asins`, {method:'GET'});
      console.log(result);
      this.setState({
        asins: result
      });
      // let details = [];
       result.map((asin) => {
        
          this.Auth.fetch(`${this.domain}/search/asin/${asin}`).then((asinDetails) => {
              console.log(asinDetails);
              let detailsCopy = [...this.state.details.slice(), asinDetails];
              //  details.copy
              this.setState({
               details: detailsCopy
             }); 
          }).catch(e => console.log(e));
        });
    } catch (e) {
      console.log(e);
    }
  }

  getAsinsDetails = async (asin) => {
    try {
      let response = await this.Auth.fetch(`${this.domain}/search/asin/${asin}`, {method: "GET"});
      return response
    } catch (e) {
      console.log(e);
    }
  }

  getTrackedAsins = async () => {
    try {
      let response = await this.Auth.fetch(`${this.domain}/tracked-asin`, {method: "GET"});
      let asinsArray = response;
      console.log('getTrackedAsins()',response);
      this.setState({
        trackedAsins: asinsArray
      });
      if(!this.state.trackedAsins.length){
        this.setState({showTracked: false, trackedMessage: "Show Tracked"})
      }
      return asinsArray;
    
    } catch (e) {
      console.log(e)
    }
  }
  
  componentWillMount() {
    // this.getAsinsList();
    console.log('componentWillMount')
    this.getTrackedAsins();
  }

  // removeAsinFromSearchResults = (asin) => {
  //   let results = this.state.searchResults;
  //   let toRemove = results.filter((item) => item.asin === asin);
  //   let index = results.indexOf(toRemove[0]);
  //   results.splice(index, 1);
  //   this.setState({
  //     searchResults: results
  //   });
  // }

  stopTrackedAsinHandler = async (id) => {
    try {
      console.log({id})
      await this.Auth.fetch(`${this.domain}/tracked-asin/`, {
        method: 'delete',
        body: JSON.stringify({
          id: id,
          })
      });
      await this.getTrackedAsins();
      
    } catch (e) {
      console.log(e);
    }
  };

  searchHandler = async (query) => {
    try {
      this.setState({
        searchResults: [],
        loading: true
      });
      let response = await this.Auth.fetch(`${this.domain}/search/${query}`, {method: 'get'});
      console.log(response);
      this.setState({
        searchResults: response,
        loading: false
      });
      
    } catch (e) {
      this.setState({
        searchResults: [],
        loading: true
      });
      console.log(e);
    }
  }

  toggleTracked = () => {
    let tracked = !this.state.showTracked;
    let message = this.state.trackedMessage;
    let newMessage = ''
    if (message === "Show Tracked") {
      newMessage = "Hide Tracked";
    }
    if(message === "Hide Tracked") {
      newMessage = "Show Tracked";
    }
    this.setState({showTracked: tracked, trackedMessage: newMessage});
    window.scrollTo(0, 0);
  }

  toggleCredentials = () => {
    let cred = !this.state.showCredentials;
    this.setState({showCredentials: cred});
  }
  
  render() {
    let price = null;
    if (this.state.prices.length > 0) {
      price = (
        this.state.prices.map((p, index) => {
          return (
          <p key={index}>${p}</p>
        )
        })
      )
    }

    let hideSearch = '';
    if (this.state.loading) {
      hideSearch = classes.hide;
    }

    let hideLoading = '';
    if (!this.state.loading) {
      hideLoading = classes.hide;
    }

    let searchForm = null;
    if(!this.state.showTracked) {
      searchForm = (
        <div className={classes.searchAndLoading}>
          <div className={hideSearch}>
            <SearchForm className={classes.search} 
              search={this.searchHandler} 
              domain={this.domain}/>
          </div>
          <div className={hideLoading}>
            <Loading />
          </div>
        </div>
      )
    }

    let credentialsForm = null;
    if(this.state.showCredentials) {
      credentialsForm = <CredentialsForm domain={this.domain}/>
    };
    
    let trackedList = null;
    if(this.state.trackedAsins && this.state.showTracked) {
      trackedList = (
          <div className={classes.trackedResultsContainer}>
              <TrackedAsins 
              trackedAsins={this.state.trackedAsins}
              stopTrackedAsinHandler={this.stopTrackedAsinHandler}/>
          </div>
      )
    }

    let searchedResults = null;
    if(this.state.searchResults.length > 0 && !this.state.showTracked) {
      searchedResults = (
        <div className={classes.searchedResult}>
          <SearchedProducts 
            getTrackedAsins={this.getTrackedAsins}
            getAsinsDetails={this.getAsinsDetails}
            searchResults={this.state.searchResults}
            domain={this.domain}
            // removeAsinFromSearchResults={this.removeAsinFromSearchResults}
            />
        </div>
      )
    }

    let trackButton = null;
    if (this.state.trackedAsins.length) {
      trackButton = (
        <div className={classes.stickyButton}>
          <button className={classes.trackedButton} onClick={this.toggleTracked}>{this.state.trackedMessage}</button>
        </div>
      )
    }

    return (
      <div className={classes.App}>
        
        <header className={classes["App-header"]}>
          <div className={classes.login}>
            <Login reset={this.resetState}
              getTracked={this.getTrackedAsins}
              domain={this.domain}/>
            {/* <button className={classes.credBtn} onClick={this.toggleCredentials}>Enter Credentials</button> */}
          </div>
          <h1 className={classes['App-title']}>Amazon Price Tracker</h1>
        </header>
        
        {/* {credentialsForm} */}
       
        {/* <Asin
            getTrackedAsins={this.getTrackedAsins}
            getAsinsDetails={this.getAsinsDetails}
            asinList={this.state.details}/> */}
        {trackButton}
        {trackedList}
        {searchForm}
        {/* {price} */}
        {searchedResults}
      </div>
    );
  }
}

export default App;
