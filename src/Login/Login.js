import React from 'react';
import AuthService from '../AuthService';
import FaTimes from 'react-icons/lib/fa/times-circle';
import isEmail from 'validator/lib/isEmail';
import blacklist from 'validator/lib/blacklist';

import classes from './Login.css';

class Login extends React.Component {
    constructor(props){
        super(props);
        
        this.state = {
            loggedIn: false,
            loginMessage: '',
            showSignup: false,
            password: '',
            username: '',
            firstName: '',
            lastName: '',
            email: '',
            phoneNumber: '',
            touched: {
                username: false,
                password: false,
                email: false,
                phoneNumber: false,
                firstName: false,
                lastName: false
              },
        };

        this.domain = this.props.domain;
        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.Auth = new AuthService(this.props.domain);
    }

    componentWillMount() {
        this.loggedIn();
    }

    loggedIn() {
        if (!this.Auth.loggedIn()) {
            this.setState({loggedIn: false})
        }
        else {
            try {
                const profile = this.Auth.getProfile()
                this.setState({
                    user: profile,
                    loggedIn: true
                })
            }
            catch(err){
                this.Auth.logout()
                this.setState({loggedIn: false})
            }
        }
    }

    handleChange(e){
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    }

    handleFormSubmit = async (e) => {

        e.preventDefault();
        try {
            const res = await this.Auth.login(this.state.username,this.state.password)
                if(!res.success) {
                    console.log(res);
                   this.setState({loginMessage: res.err.message});
                } else {
                    console.log(res);

                    //    this.props.history.replace('/');
                    // console.log(this.Auth.isTokenExpired(res.token));
                    // console.log(this.Auth.getProfile(res.token));
                    const profile = await this.Auth.getProfile()
                    this.setState({
                        user: profile,
                        loggedIn: true,
                        password: ''
                    });
                    this.setState({loginMessage: res.status});  
                    this.props.getTracked();
                }
                setTimeout(()=> {
                    this.setState({loginMessage: ''});  
                },2000)
                     
            } catch (err) {
                console.log(err)
            }
    }

    handleSignup = async (e) => {
        e.preventDefault();
        try {
            await this.Auth.fetch(`${this.domain}/users/signup`, {
            method: 'POST',
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                email: this.state.email,
                phoneNumber: this.state.phoneNumber
                })
            });
            await this.handleFormSubmit(e);
        } catch (e) {
            console.log(e);
        }
    }

    toggleSignup() {
        // console.log(this.state)
        let signup = this.state.showSignup;
        this.setState({
            showSignup: !signup,
            touched: {
                username: false,
                password: false,
                email: false,
                phoneNumber: false,
                firstName: false,
                lastName: false
              },
        });
    }

    handleLogout(e) {
        console.log(this.Auth.logout())
        this.Auth.logout();
        this.setState({
            loggedIn: false, 
            showSignup: false,
            user: '', 
            username: '',
            touched: {
                username: false,
                password: false,
                email: false,
                phoneNumber: false,
                firstName: false,
                lastName: false
              },
            });
        this.props.reset();
    }

    validate(username, password, firstName, lastName, email, phoneNumber) {
        // const { username, password } = this.state;
        // true means invalid, so our conditions got reversed
        if (!this.state.showSignup) {
            return {
                username: username.length < 3,
                password: password.length < 3,
              };
        } else if(this.state.showSignup){
            let phone = blacklist(phoneNumber, '\\.\\(\\)\\-')
            console.log(phone)
            return {
                username: username.length < 3,
                password: password.length < 3,
                firstName: firstName.length < 3,
                lastName: lastName.length < 3,
                email: email.length < 3 || !isEmail(email), 
                phoneNumber: phone.length < 10 || isNaN(phone)
            }
        }
    }

    handleBlur = (field) => (event) => {
        this.setState({
          touched: { ...this.state.touched, [field]: true },
        });
    }

    shouldMarkError = (field) => {
        const errors = this.validate(this.state.username, this.state.password, this.state.firstName, this.state.lastName, this.state.email, this.state.phoneNumber);
        const hasError = errors[field];
        const shouldShow = this.state.touched[field];
        
        return hasError ? shouldShow : false;
    };
    
    render() {
        
        const { username, password, firstName, lastName, email, phoneNumber } = this.state;
        // const isEnabled = username.length > 0 && password.length > 0;
        const errors = this.validate(username, password, firstName, lastName, email, phoneNumber);
        const isEnabled = !Object.keys(errors).some(x => errors[x]);

        let login = null;
        if (!this.state.loggedIn) {
            login = (
                <form className={classes.form} onSubmit={this.handleFormSubmit}>
                    <div className={classes.login}>
                        <label>Username:</label>
                        <input 
                            type="text" autoComplete="off"
                            className={this.shouldMarkError('username') ? classes.error + ' ' + classes.signInInput : classes.signInInput}
                            name="username" placeholder="Username" 
                            onChange={this.handleChange}
                            onBlur={this.handleBlur('username')}/>
                    </div>
                    <div className={classes.login}>
                        <label>Password:</label>
                        <input type="password" autoComplete="off"
                        className={this.shouldMarkError('password') ? classes.error + ' ' + classes.signInInput : classes.signInInput}
                        name="password" placeholder="Password" 
                        onChange={this.handleChange}
                        onBlur={this.handleBlur('password')}/>
                    </div>
                    <div>
                        <button className={isEnabled ? classes.loginButton : classes.loginButtonDisabled} disabled={!isEnabled} type="submit">Login</button>
                    </div>
                    <p className={classes.showSignup} onClick={() => this.toggleSignup()}>SignUp</p>
                </form>
            )
        }

        let signup = null;
        if (!this.state.loggedIn && this.state.showSignup) {
            signup = (
                <div className={classes.signupBackground}>
                <form className={classes.signUpContainer} onSubmit={this.handleSignup}>

                    <div>
                        <p className={classes.signupTitle}>Sign Up</p>
                        <span className={classes.icon} onClick={() => this.toggleSignup()}><FaTimes /></span>
                    </div>
                    
                    <div className={classes.inputDiv}>
                        <label>Username:</label>
                        <input className={this.shouldMarkError('username') ? classes.signupError + ' ' + classes.input : classes.input} 
                            type="text" name="username" placeholder="Username" 
                            onChange={this.handleChange}
                            onBlur={this.handleBlur('username')}/>
                    </div>

                    <div className={classes.inputDiv}>
                        <label>Password:</label>
                        <input className={this.shouldMarkError('password') ? classes.signupError + ' ' + classes.input : classes.input} 
                            type="password" name="password" placeholder="Password" 
                            onChange={this.handleChange}
                            onBlur={this.handleBlur('password')}/>
                    </div>

                    <div className={classes.inputDiv}>
                        <label>First Name:</label>
                        <input className={this.shouldMarkError('firstName') ? classes.signupError + ' ' + classes.input : classes.input} 
                            type="text" name="firstName" placeholder="First Name" 
                            onChange={this.handleChange}
                            onBlur={this.handleBlur('firstName')}/>
                    </div>
                    <div className={classes.inputDiv}>
                        <label>Last Name:</label>
                        <input className={this.shouldMarkError('lastName') ? classes.signupError + ' ' + classes.input : classes.input} 
                            type="text" name="lastName" placeholder="Last Name" 
                            onChange={this.handleChange}
                            onBlur={this.handleBlur('lastName')}/>
                    </div>
                    <div className={classes.inputDiv}>
                        <label>Email:</label>
                        <input className={this.shouldMarkError('email') ? classes.signupError + ' ' + classes.input : classes.input}  
                            type="text" name="email" placeholder="Email" 
                            onChange={this.handleChange}
                            onBlur={this.handleBlur('email')}/>
                    </div>
                    <div className={classes.inputDiv}>
                        <label>Phone:</label>
                        <input className={this.shouldMarkError('phoneNumber') ? classes.signupError + ' ' + classes.input : classes.input} 
                            type="text" name="phoneNumber" placeholder="Phone" 
                            onChange={this.handleChange}
                            onBlur={this.handleBlur('phoneNumber')}/>
                    </div>
                    
                    {/* For future use */}
                    {/* <p className={classes.signupTitle}>Amazon Seller Account Information:</p>
                    <div className={classes.inputDiv}>
                        <label>Seller ID:</label>
                        <input className={classes.input} type="text" name="sellerID" placeholder="Seller ID" onChange={this.handleChange}/>
                    </div>

                    <div className={classes.inputDiv}>
                        <label>Auth Token:</label>
                        <input className={classes.input} type="text" name="authToken" placeholder="Auth Token" onChange={this.handleChange}/>
                    </div>
                    <div className={classes.inputDiv}>
                        <label>Marketplace ID:</label>
                        <input className={classes.input} type="text" name="marketplaceID" placeholder="Marketplace ID" onChange={this.handleChange}/>
                    </div> */}
                    
                    <div className={classes.btnDiv}>
                        <button className={isEnabled ? classes.signUpButton : classes.signUpButtonDisabled} type="submit" disabled={!isEnabled}>Sign Up</button>
                    </div>
                </form>
                </div>
            )
        }

        let loginMessage = null;
        let message = ''
        if (this.state.loginMessage) {
            if (this.state.loginMessage === 'Login Successful!'){
                message = classes.loginMessage;
            } else {
                message = classes.loginMessage + ' ' + classes.loginErrorMessage;
            }
            loginMessage = (
                <div className={message}>
                    <h3>{this.state.loginMessage}</h3>
                </div>
            )
        }

        let logout = null;
        if(this.state.loggedIn) {
            logout = (
                <div className={classes.form}>
                    {/* <p className={classes.p}>Hello {this.state.username}</p> */}
                    <div className={classes.login}>
                    <button className={classes.logoutButton} type="submit" onClick={() => this.handleLogout()}>Logout</button>
                    </div>
                    
                </div>
            )
        }

        return (
            <div>
                {signup}
                {login}
                {loginMessage}
                {logout}
            </div>
            
        )
    }
    
}

export default Login;