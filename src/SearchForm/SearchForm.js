import React from 'react';
import classes from './SearchForm.css';
import MdSearch from 'react-icons/lib/md/search'

class SearchForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchTerm: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            searchTerm: event.target.value
        });
    }

    handleSubmit = async (event) =>  {
        event.preventDefault();
        if (this.state.searchTerm.trim() === ''){
            return
        } else {
            await this.props.search(this.state.searchTerm.trim());
            this.setState({searchTerm: ''});
        }
    }

    render() {
        return (
            <form className={classes.form} onSubmit={this.handleSubmit}>
                <div className={classes.container}>
                    <span className={classes.icon}><MdSearch /></span>
                    <input type="text" className={classes.searchInput} name="searchTearm" value={this.state.searchTerm} placeholder="Search..." onChange={this.handleChange}/>
                </div>
                {/* <button className={classes.searchBtn} type="submit">Search</button> */}
                
            </form>
        )
    }
    
}

export default SearchForm;