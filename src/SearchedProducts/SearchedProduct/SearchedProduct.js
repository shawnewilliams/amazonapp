import React from 'react';
// import MdAccessAlarm from 'react-icons/lib/md/access-alarm';
import MdAttachMoney from 'react-icons/lib/md/attach-money';

import classes from './SearchedProduct.css';
import AuthService from '../../AuthService';

class SearchedProduct extends React.Component {
    constructor(props) {
        super(props);

        this.Auth = new AuthService(this.props.domain);
        this.domain = this.props.domain;
        this.state = {
            asin: this.props.asin,
            hours: '',
            minPrice: '',
            alert: false,
            trackedAlert: false,
            touched: {
                minPrice: false,
                hours: false,
              },
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        try {
            if(!this.Auth.loggedIn()) {
                this.setState({alert: true})
                setTimeout(() => {
                    this.setState({alert: false})
                }, 2500);
                return
            }
            this.setState({trackedAlert: true})
                setTimeout(() => {
                    this.setState({trackedAlert: false})
                }, 2500);
            // this.props.removeAsinFromSearchResults(this.props.asin)
            let toTrack = await this.props.getAsinsDetails(this.props.asin);
            console.log('toTrack', toTrack);
            const response = await this.Auth.fetch(`${this.domain}/tracked-asin`, {
                method: 'POST',
                body: JSON.stringify({
                    asin: toTrack.asin,
                    image: toTrack.image,
                    price: toTrack.price,
                    title: toTrack.title,
                    minPrice: this.state.minPrice,
                    hours: this.state.hours
                })
            })
       
            await this.props.getTrackedAsins();

            let newPrices = response;
            this.setState({
                asin: '',
                minPrice: '',
                hours: ''
            });
        } catch (error) {
            this.setState({
                asin: '',
                minPrice: '',
                hours: ''
            });
        console.log(error);
        }
      };

      validate(minPrice) {
        // true means invalid, so our conditions got reversed
        return {
            minPrice: isNaN(minPrice) || minPrice.length === 0,
            // hours: isNaN(hours) || hours.length === 0,
        }
    }

    handleBlur = (field) => (event) => {
        this.setState({
          touched: { ...this.state.touched, [field]: true },
        });
    }

    shouldMarkError = (field) => {
        const errors = this.validate(this.state.minPrice);
        const hasError = errors[field];
        const shouldShow = this.state.touched[field];
        
        return hasError ? shouldShow : false;
    };
   
    render(){

        const { minPrice, hours } = this.state;
        const errors = this.validate(minPrice, hours);
        const isEnabled = !Object.keys(errors).some(x => errors[x]);

        // let displayPrice = null;
        // if(this.props.price !== 'Unavailable') {
        //     displayPrice = (
        //         <p>Price: ${this.props.price}</p>
        //     )
        // };

        let alert = null
        if(this.state.alert) {
            alert = (
                <div className={classes.alert}>
                    <h4>You must be logged in to track products.</h4>
                </div>
            )
        }

        let trackedAlert = null
        if(this.state.trackedAlert) {
            alert = (
                <div className={classes.alert + ' ' + classes.trackedAlert}>
                    <h4>Successfully tracking ASIN {this.props.asin}</h4>
                </div>
            )
        }

        return (
            <div className={classes.card}>
                {alert}
                <h4 className={classes.asin}>ASIN: {this.props.asin}</h4>
                
                <div className={classes.details}>
                    <iframe className={classes.iframe}  scrolling="no" frameBorder="0" title={`Amazon Product ${this.props.asin}`} src={`//ws-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=US&source=ac&ref=tf_til&ad_type=product_link&tracking_id=108f29-20&marketplace=amazon&region=US&placement=${this.props.asin}&asins=${this.props.asin}&linkId=903821764170d5545bd428917461f09e&show_border=false&link_opens_in_new_window=true&price_color=000000&title_color=0066C0&bg_color=FFFFFF`}></iframe>
                    {/* <a href={`https://www.amazon.com/dp/${this.props.asin}`} target="black"><img className={classes.smallImage} src={this.props.image} /></a> */}
                    <div className={classes.detailsList}>
                        {/* <p>ASIN: {this.props.asin}</p> */}
                        {/* <p>{this.props.title}</p> */}
                        {/* {displayPrice} */}
                    </div>
                    
                </div>
            
                <form className={classes.form} onSubmit={this.handleSubmit}>
                    <div className={classes.inputDiv}>
                        <span className={classes.icon}><MdAttachMoney /></span>
                        <input className={classes.input} 
                            type="text" name="minPrice" autoComplete="off"
                            value={this.state.minPrice} placeholder="Min Price" 
                            onChange={this.handleChange} />
                    </div>
    
                    {/* <div className={classes.inputDiv}>
                        <span className={classes.icon}><MdAccessAlarm /></span>
                        <input className={classes.input} 
                            type="text" name="hours" autoComplete="off"
                            value={this.state.hours} placeholder="Hours" 
                            onChange={this.handleChange} />
                    </div> */}
                    
                    <div className={classes.btnDiv}>
                        <button className={!isEnabled ? classes.disabled : classes.button} type="submit" disabled={!isEnabled}>Start</button>
                    </div>
                </form>
               
            </div>
        )
    }
    
}

export default SearchedProduct;