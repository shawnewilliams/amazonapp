import React from 'react';
import SearchedProduct from './SearchedProduct/SearchedProduct';

const searchedProducts = (props) => props.searchResults.map((item, index) => {
    return (
      <SearchedProduct key={index}
        // removeAsinFromSearchResults={props.removeAsinFromSearchResults}
        getTrackedAsins={props.getTrackedAsins}
        getAsinsDetails={props.getAsinsDetails}
        asin={item.asin}
        image={item.image}
        price={item.price}
        title={item.title}
        domain={props.domain}
        // setAsin={props.setAsinCustomHandler} 
        // setAsinValue={props.asinCustom}
        // setMinPrice={props.setMinPriceCustomHandler} 
        // setMinPriceValue={props.minPriceCustom}
        // setHours={props.setHoursCustomHandler} 
        // setHoursValue={props.hoursCustom}
        // getPrices={() => props.getPricesCustomHandler(item.asin)}
         />
    )
  })

export default searchedProducts;